package LinearAlgebra;
/**
 * Davit Voskerchyan
 * Student Id:2034100
 * 
 */
public class Vector3d{
    private double x,y,z;

    public Vector3d(double newX, double newY, double newZ){
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }
 
    /** Method Magnitude()
     * This method calculates the magnitude.
     * 
     * @return magnitude : Sqrt(x^2+y^2+z^2).
     */
    public double magnitude(){
        double sum= ( Math.pow(this.x, 2)+ Math.pow(this.y, 2)+Math.pow(this.z, 2) );
        double magnitude = Math.sqrt(sum);
        return magnitude;
    }

    /** Method dotProduct
     * This method calcualte teh dot product of two vectors.
     * 
     * @param vector : another vector
     * @return dotProduct : Each cooresponding coordinates multiplyed by the other vector
     */
    public double dotProduct(Vector3d vector){
        double dotProduct = (this.x*vector.x)+(this.y*vector.y)+(this.z*vector.z);
        return dotProduct;
    }

    public Vector3d add(Vector3d vector){
        Vector3d newVector;
        double x,y,z;

        x = this.x + vector.x;
        y = this.y + vector.y;
        z = this.z + vector.z;

        newVector = new Vector3d(x,y,z);
        return newVector;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
}