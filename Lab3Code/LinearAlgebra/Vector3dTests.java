package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
/**
 * Davit Voskerchyan
 * Student Id:2034100
 * 
 */
public class Vector3dTests {
    @Test
    public void getMethodTestX(){
        Vector3d vector = new Vector3d(3,5,7);
        assertEquals(vector.getX(),3);
        assertEquals(vector.getY(),5);
        assertEquals(vector.getZ(),7);
    }
    @Test
    public void getMethodTestY(){
        Vector3d vector = new Vector3d(3,12,7);
        assertEquals(vector.getY(),12);
    }
    @Test
    public void getMethodTestZ(){
        Vector3d vector = new Vector3d(3,5,-0);
        assertEquals(vector.getZ(),0);
    }
    @Test
    public void magnitudeTest(){
        Vector3d vector = new Vector3d(12,-1,0);
        assertEquals(vector.magnitude(), 12.04, 0.1);
    }
    @Test
    public void dotProductTest(){
        Vector3d vector = new Vector3d(12, -3, 9);
        Vector3d vector2 = new Vector3d(-2, 11, 17);
        assertEquals(vector.dotProduct(vector2), 96);
    }
    @Test
    public void addTestX(){
        Vector3d vector = new Vector3d(-5, -3, -9);
        Vector3d vector2 = new Vector3d(0, 8, 2);
        assertEquals(vector.add(vector2).getX(), -5);
    }
    @Test
    public void addTestY(){
        Vector3d vector = new Vector3d(-5, -1, -9);
        Vector3d vector2 = new Vector3d(0, 4, 2);
        assertEquals(vector.add(vector2).getY(), 3);
    }
    @Test
    public void addTestZ(){
        Vector3d vector = new Vector3d(-5, -3, 12);
        Vector3d vector2 = new Vector3d(0, 8, -23);
        assertEquals(vector.add(vector2).getZ(), -11);
    }
}
